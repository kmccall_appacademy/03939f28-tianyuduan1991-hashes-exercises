# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hashie = Hash.new
  str.split(" ").each do |i|
hashie[i] = i.length
  end
  hashie
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
hash.key(hash.values.max)
end


# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
     older[key] = value
   end

   older
end
# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.


def letter_counts(word)
hashie = Hash.new(0)
word.chars.each do |val|
hashie[val] += 1

end
hashie
end


# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
hashie = Hash.new(0)
arr.each do |val|
  hashie[val] +=1
end
hashie.keys
end


# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hashie = {:even => 0, :odd => 0}
numbers.each do |i|
  if i.even?
hashie[:even] += 1
  elsif i.odd?
  hashie[:odd] += 1
end
end
 hashie
end


# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hashie = {"a" => 0, "e" => 0, "i" => 0, "o" => 0, "u" => 0}
  string.chars do |letter|
    if letter.include?("a")
      hashie["a"] += 1
    elsif letter.include?("e")
      hashie["e"] += 1
    elsif letter.include?("i")
      hashie["i"] += 1
    elsif letter.include?("o")
      hashie["o"] += 1
    elsif letter.include?("u")
      hashie["u"] += 1
    end

  end
 # hashie.key(hashie.values.max)
 max_value = hashie.max_by {|k,v| v}[0] #Returns the object in enum with the maximum value

end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fall_babies = students.select {|k,v| v > 7}.keys
output_arr = []
  fall_babies.each_index do |idx1|
    ((idx1 + 1..fall_babies.length - 1)).each do |idx2|
      output_arr << [fall_babies[idx1],fall_babies[idx2]]
    end
  end
   output_arr
end
fall_and_winter_birthdays(students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
 "Dottie" => 8, "Warren" => 9 })

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
number_of_species = specimens.uniq.length



hashie = Hash.new(0)
specimens.each do |animal|
hashie[animal] += 1
end

smallest_population_size = hashie.values.min
largest_population_size = hashie.values.max

biodiversity_index  = number_of_species**2 * smallest_population_size / largest_population_size
end
biodiversity_index(["cat", "leopard-spotted ferret", "dog"])

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
 vandalized_count = character_count(vandalized_sign)

 vandalized_count.all? do |character, count|
   normal_count[character.downcase] >= count
 end

end

def character_count(str)
  hashie= Hash.new(0)
  str.chars do |letter|
    next if letter == " "
    hashie[letter.downcase] += 1
  end
hashie
end
